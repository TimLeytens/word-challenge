# 6 letter words

## Assignment

There's a file in the root of the repository, input.txt, that contains words of varying lengths (1 to 6 characters).

Your objective is to show all combinations of those words that together form a word of 6 characters. That combination must also be present in input.txt
E.g.:  

<code>foobar  
fo  
obar</code>

should result in the ouput:  

<code>fo+obar=foobar</code>

You can start by only supporting combinations of two words and improve the algorithm at the end of the exercise to support any combinations.

Treat this exercise as if you were writing production code; think unit tests, SOLID, clean code and avoid primitive obsession. Be mindful of changing requirements like a different maximum combination length, or a different source of the input data.

The solution must be stored in a git repo. After the repo is cloned, the application should be able to run with one command / script.

Don't spend too much time on this.

## Installation

Clone or download this repository and run 'ConsoleApp.exe', make sure the input.txt file is in the same folder as the executable.

## Enhancements

* Add maximumn combination lenght to a configuration and allow to be passed as a runtime argument
* Add filename to a configuration and allow to be passed as a runtime argument
* Allow more then two word combinations by adding a new ICombinationProvider implementation
