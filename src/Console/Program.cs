﻿using Application;
using ConsoleApp;
using Domain.ValueObjects;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

using IHost host = Host.CreateDefaultBuilder(args)
    .ConfigureServices((_, services) => services.AddApplicationServices())
    .Build();

var challenge = host.Services.GetService<WordChallenge>();
IEnumerable<Word> words = challenge?.FindAll() ?? new List<Word>();
foreach (var word in words)
{
    Console.WriteLine(word);
}

Console.WriteLine($"Total results: {words.Count()}");

await host.RunAsync();