﻿namespace Domain.Exceptions;
public class WordLengthException : Exception
{
    public WordLengthException(int min, int max) : base($"Word length should be equal or larger then {min} and smaller or equal to {max}")
    {
    }
}
