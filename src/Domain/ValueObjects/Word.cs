﻿using Domain.Exceptions;

namespace Domain.ValueObjects;
public record Word
{

    public string Value { get; init; }

    private const int minimumWordLength = 1;
    private const int maximumWordLength = 6;

    public Word(string value)
    {
        if (!IsValid(value))
        {
            throw new WordLengthException(minimumWordLength, maximumWordLength);
        }

        Value = value;
    }

    public override string ToString()
    {
        return Value;
    }

    private static bool IsValid(string word)
    {
        return word.Length >= minimumWordLength && word.Length <= maximumWordLength;
    }
}