﻿using Domain.ValueObjects;

namespace Application.Repositories;
public interface IWordRepository
{
    IEnumerable<Word> GetWords();
}
