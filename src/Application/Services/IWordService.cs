﻿using Domain.ValueObjects;

namespace Application.Services;
public interface IWordService
{
    IDictionary<int, HashSet<Word>> GetWords();
}
