﻿namespace Application.Common;
public interface IFileReader
{
    IEnumerable<string> Read(string filename);
}
