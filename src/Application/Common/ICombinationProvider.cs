﻿using Domain.ValueObjects;
namespace Application.Common;
public interface ICombinationProvider
{
    public HashSet<Word> GetCombinations(IDictionary<int, HashSet<Word>> dictionary, int maxLength);
}
