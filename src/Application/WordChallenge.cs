﻿using Application.Common;
using Application.Services;
using Domain.ValueObjects;

namespace Application;
public class WordChallenge
{
    private readonly IWordService _wordService;
    private readonly ICombinationProvider _combinationProvider;
    private const int maximumWordLength = 6;

    public WordChallenge(IWordService wordService, ICombinationProvider combinationProvider)
    {
        _wordService = wordService;
        _combinationProvider = combinationProvider;
    }

    public IEnumerable<Word> FindAll()
    {
        var dictionary = _wordService.GetWords();
        var combinations = _combinationProvider.GetCombinations(dictionary, maximumWordLength);
        var control = new HashSet<Word>();
        if (dictionary.ContainsKey(maximumWordLength))
        {
            control = dictionary[maximumWordLength];
        }
        var result = combinations.Intersect(control);
        return result;
    }
}
