﻿using Application.Common;
using Application.Repositories;
using Domain.ValueObjects;

namespace Infrastructure.Repositories;
public class WordRepository : IWordRepository
{
    private readonly IFileReader _fileReader;

    public WordRepository(IFileReader fileReader)
    {
        _fileReader = fileReader;
    }

    public IEnumerable<Word> GetWords()
    {
        return _fileReader.Read(@"input.txt").Select(x => new Word(x)).ToList();
    }
}
