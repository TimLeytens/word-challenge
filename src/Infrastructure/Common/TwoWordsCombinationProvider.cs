﻿using Application.Common;
using Domain.ValueObjects;

namespace Infrastructure.Common;
public class TwoWordsCombinationProvider : ICombinationProvider
{
    public HashSet<Word> GetCombinations(IDictionary<int, HashSet<Word>> dictionary, int maxLength)
    {
        var result = new HashSet<Word>();

        int remainderKey;

        for (int i = 1; i < maxLength; i++)
        {

            remainderKey = maxLength - i;

            if (i > remainderKey) break;

            if (!KeyNotEmpty(dictionary, i) || !KeyNotEmpty(dictionary, remainderKey)) continue;

            foreach (var word1 in dictionary[i])
            {
                foreach (var word2 in dictionary[remainderKey])
                {
                    result.Add(new(word1.Value + word2.Value));
                    result.Add(new(word2.Value + word1.Value));
                }
            }
        }
        return result;
    }

    private static bool KeyNotEmpty(IDictionary<int, HashSet<Word>> dictionary, int key)
    {
        return dictionary.ContainsKey(key) && dictionary[key].Count > 0;
    }
}