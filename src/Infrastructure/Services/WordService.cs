﻿using Application.Repositories;
using Application.Services;
using Domain.ValueObjects;

namespace Infrastructure.Services;
public class WordService : IWordService
{
    private readonly IWordRepository _wordRepository;

    public WordService(IWordRepository wordRepository)
    {
        _wordRepository = wordRepository;
    }

    public IDictionary<int, HashSet<Word>> GetWords()
    {
        var output = new Dictionary<int, HashSet<Word>>();
        var words = _wordRepository.GetWords();
        foreach (var word in words)
        {
            var wordLenght = word.Value.Length;
            if (!output.ContainsKey(wordLenght))
            {
                output.Add(wordLenght, new HashSet<Word>());
            }
            output[wordLenght].Add(word);
        }
        return output;
    }
}
