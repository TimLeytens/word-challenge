﻿namespace Domain.Tests.ValueObjects;

public class WordTest
{
    [Fact]
    public void TestWordEquality()
    {
        Word word1 = new("Word");
        Word word2 = new("Word");
        Assert.Equal(word1, word2);
    }

    [Fact]
    public void TestWordInequality()
    {
        Word word1 = new("Word");
        Word word2 = new("Faulty");
        Assert.NotEqual(word1, word2);
    }

    [Fact]
    public void TestWordLengthMin()
    {
        Assert.Throws<WordLengthException>(() => new Word(""));
    }

    [Fact]
    public void TestWordLengthMax()
    {
        Assert.Throws<WordLengthException>(() => new Word("1234567"));
    }
}
